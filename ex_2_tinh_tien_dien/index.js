

function tinhTienDien() {
    var tongTienDien = 0;
    var ten = document.getElementById("ten").value;
    var kwDien = document.getElementById("txt-kw-dien").value * 1;
    if (kwDien <= 50) {
        tongTienDien = kwDien * 500;
    } else if (kwDien <= 100) {
        tongTienDien = 50 * 500 + (kwDien - 50) * 650;
    } else if (kwDien <= 200) {
        tongTienDien = 50 * 500 + 50 * 650 + (kwDien - 100) * 850;
    } else if (kwDien <= 350) {
        tongTienDien = 50 * 500 + 50 * 650 + 100 * 850 + (kwDien - 200) * 1100;
    } else {
        tongTienDien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100+(kwDien - 350) * 1300;
    }
    document.getElementById("result").innerHTML = `Họ tên: ${ten}; Tiền điện: ${tongTienDien}`

}